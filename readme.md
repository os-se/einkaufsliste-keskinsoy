# Lebenslauf

**Name** Can Keskinsoy<br>
**Telefon** 0123 98 87 765<br>
**Email** can.keskinsoy@hfu.de<br>
**Geburtsjahr** 01.01.1900<br>

## Berufserfahrung

| Jahr        | Berufsbezeichnung                       | Unternehmen |
| :---------- | :-------------------------------------- | :---------- |
| 2016 - 2023 | Director of Flight Projects Directorate | NASA        |
| 2010 - 2016 | General Engineer                        | Tesla       |
| 2008 - 2010 | Botanist                                | Blumenmaier |

## Ausbildung

| Jahr | Abschluss           | Ort        |
| :--- | :------------------ | :--------- |
| 2008 | Hauptschulabschluss | Furtwangen |
